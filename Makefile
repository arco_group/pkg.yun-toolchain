# -*- mode: makefile-gmake; coding: utf-8 -*-

# libs goes to: /usr/mips-linux-gnu/lib
# headers in:   /usr/mips-linux-gnu/include

SHELL         = /bin/bash
DEST          = yun-toolchain

TCNAME        = toolchain-mips_r2_gcc-4.6-linaro_uClibc-0.9.33.2
UCNAME        = target-mips_r2_uClibc-0.9.33.2
INSTALL_DIR   = $(DEST)-install/pkg
STAGING_DIR   = $(DEST)/staging_dir
TOOLCHAIN_DIR = $(STAGING_DIR)/$(TCNAME)
YUN_REPO      = https://github.com/arduino/openwrt-yun.git

all:

clone-repo:
	[ -d $(DEST) ] || git clone $(YUN_REPO) $(DEST)

build-toolchain:
	cp config $(DEST)/.config
	make -C $(DEST) -j \
		tools/install \
		toolchain/install \
	        package/opkg/host/install \
		package/toolchain/install \
		package/uclibc++/install

copy-important-files: TCDIR = $(INSTALL_DIR)/staging_dir/$(TCNAME)
copy-important-files: UCDIR = $(INSTALL_DIR)/staging_dir/$(UCNAME)
copy-important-files:
	install -d $(INSTALL_DIR)
	cp -r debian.in $(INSTALL_DIR)/debian

	install -d $(TCDIR)
	cp -r $(TOOLCHAIN_DIR)/bin $(TCDIR)
	cp -r $(TOOLCHAIN_DIR)/include $(TCDIR)
	cp -r $(TOOLCHAIN_DIR)/lib $(TCDIR)
	cp -r $(TOOLCHAIN_DIR)/libexec $(TCDIR)
	rm $(TCDIR)/bin/ldd

	ln -fs mips-openwrt-linux-uclibc-as $(TCDIR)/bin/as

	install -d $(TCDIR)/mips-openwrt-linux
	cp -r $(TOOLCHAIN_DIR)/mips-openwrt-linux/include $(TCDIR)/mips-openwrt-linux

	install -d $(INSTALL_DIR)/usr/include
	cp yun.mk $(INSTALL_DIR)/usr/include

	install -d $(UCDIR)/usr
	cp -r $(STAGING_DIR)/$(UCNAME)/usr/lib $(UCDIR)/usr

prepare-package: clone-repo build-toolchain

debian-package: copy-important-files
	cd $(INSTALL_DIR) && ian build

debian-install:
	cd $(INSTALL_DIR) && ian install

clean:
	rm -rf $(DEST)-install *.tar.gz

clean-all: clean
	rm -rf $(DEST)
