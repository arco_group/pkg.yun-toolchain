
About
=====

This is a Debian package to include the OpenWRT toolchain used in the
Arduino Yún. This allows you to install all needed software to develop
in C++ for the embedded MIPS.

How to Install
==============

A compiled version of this package is available from
http://babel.esi.uclm.es. You just need to add the following line to
/etc/apt/sources.list:

    deb http://babel.esi.uclm.es/arco sid main

After that, update and install `yun-toolchain`:

    $ sudo apt-get update && sudo apt-get install yun-toolchain

How to Use
==========

You just need to include "yun.mk" in your Makefile. See
"hello-world" in examples directory for a very basic example.

How to build toolchain
======================

**Note**: you may need to install the Debian package called
`ian`. Moreover, a 32 bit release is expected to work better. To build
the toolchain, just, do the following:

    $ make prepare-package debian-package

Packages
========

You may need to install some packages on your YÚN. Please see the
Downloads section to get those packages.

References
==========

* https://github.com/arduino/openwrt-yun

