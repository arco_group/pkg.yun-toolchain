# -*- mode: makefile-gmake; coding: utf-8 -*-

# libs goes to: /usr/mips-linux-gnu/lib
# headers in:   /usr/mips-linux-gnu/include

STAGING_DIR   = /usr/share/yun-toolchain
TOOLCHAIN     = $(STAGING_DIR)/toolchain-mips_r2_gcc-4.6-linaro_uClibc-0.9.33.2
GCCINCLUDE    = $(TOOLCHAIN)/mips-openwrt-linux/include/c++/4.6.3
PATH         := $(TOOLCHAIN)/bin:$(PATH)

export PATH
export STAGING_DIR

LDFLAGS       = -L$(TOOLCHAIN)/lib \
		-L$(STAGING_DIR)/target-mips_r2_uClibc-0.9.33.2/usr/lib \
		-L/usr/mips-linux-gnu/lib

CXX           = mips-openwrt-linux-uclibc-g++
CXXFLAGS      = --sysroot=$(TOOLCHAIN) -I. \
		-I$(TOOLCHAIN)/include \
		-I/usr/mips-linux-gnu/include \
		-I$(GCCINCLUDE) \
		-I$(GCCINCLUDE)/mips-openwrt-linux-uclibc

STRIP         = mips-openwrt-linux-uclibc-strip

